# MangaDex@Home - Client Benchmarks

All benchmarks were performad on GitLab CI Runners (Docker) which may vary in performance and resources.
The requests/second were measured with apache-bench over a variety of concurrent connections without keep-alive.
The nginx values are added for reference only (static image hosting with a self-signed certificate => short chains in the certificate improve TLS speed).

Results from [2021-08-04 • 347514380](https://gitlab.com/mangadex-network/client-benchmark/-/pipelines/347514380)

## SSL-Enabled (HTTPS)

| Client                                                                                                          | 10× Connections    | 100× Connections   | 200× Connections   |
| --------------------------------------------------------------------------------------------------------------- | -----------------: | -----------------: | -----------------: |
| ![](https://img.shields.io/static/v1?label=md@cloud&message=v2.0.1-1&color=blue&logo=node.js&logoColor=#339933) |   `150.11`   req/s |    `96.84`   req/s |   `140.67`   req/s |
| ![](https://img.shields.io/static/v1?label=cheetah&message=4c1f70&color=blue&logo=go&logoColor=00ADD8)          |   `189.30`   req/s |   `187.84`   req/s |   `176.66`   req/s |
| ![](https://img.shields.io/static/v1?label=mdath-go&message=v1.11.6&color=blue&logo=go&logoColor=00ADD8)        |   `139.58`   req/s |   `139.77`   req/s |   `138.27`   req/s |
| ![](https://img.shields.io/static/v1?label=scalpel&message=e3f024&color=blue&logo=rust&logoColor=000000)        | __`259.75`__ req/s | __`260.50`__ req/s | __`256.43`__ req/s |
| ![](https://img.shields.io/static/v1?label=mdath-rs&message=v0.5.3&color=blue&logo=rust&logoColor=000000)       |   `195.23`   req/s |   `196.91`   req/s |   `190.92`   req/s |
| ![](https://img.shields.io/static/v1?label=kmdah&message=v0.7.0&color=blue&logo=java&logoColor=orange)          |      `N/A`   req/s |      `N/A`   req/s |      `N/A`   req/s |
| ![](https://img.shields.io/static/v1?label=mdath&message=v2.0.1&color=blue&logo=java&logoColor=orange)          |   `111.26`   req/s |   `127.68`   req/s |   `135.67`   req/s |
| ![](https://img.shields.io/static/v1?label=mdathx&message=5ed4ec&color=blue&logo=nginx&logoColor=009639)        |   `266.10`   req/s |   `264.64`   req/s |   `258.47`   req/s |
| ![](https://img.shields.io/static/v1?label=nginx&message=v1.18.0&color=blue&logo=nginx&logoColor=009639)        |   `289.00`   req/s |   `284.75`   req/s |   `287.21`   req/s |

## SSL-Disabled (HTTP)

| Client                                                                                                          | 10× Connections     | 100× Connections    | 200× Connections    |
| --------------------------------------------------------------------------------------------------------------- | ------------------: | ------------------: | ------------------: |
| ![](https://img.shields.io/static/v1?label=md@cloud&message=v2.0.1-1&color=blue&logo=node.js&logoColor=#339933) |       `N/A`   req/s |       `N/A`   req/s |       `N/A`   req/s |
| ![](https://img.shields.io/static/v1?label=cheetah&message=4c1f70&color=blue&logo=go&logoColor=00ADD8)          | __`2122.03`__ req/s | __`1968.29`__ req/s | __`1844.87`__ req/s |
| ![](https://img.shields.io/static/v1?label=mdath-go&message=v1.11.6&color=blue&logo=go&logoColor=00ADD8)        |       `N/A`   req/s |       `N/A`   req/s |       `N/A`   req/s |
| ![](https://img.shields.io/static/v1?label=scalpel&message=e3f024&color=blue&logo=rust&logoColor=000000)        |    `777.18`   req/s |   `1126.78`   req/s |   `1103.53`   req/s |
| ![](https://img.shields.io/static/v1?label=mdath-rs&message=v0.5.3&color=blue&logo=rust&logoColor=000000)       |    `527.57`   req/s |    `547.68`   req/s |    `548.08`   req/s |
| ![](https://img.shields.io/static/v1?label=kmdah&message=v0.7.0&color=blue&logo=java&logoColor=orange)          |    `185.83`   req/s |    `254.83`   req/s |    `272.42`   req/s |
| ![](https://img.shields.io/static/v1?label=mdath&message=v2.0.1&color=blue&logo=java&logoColor=orange)          |       `N/A`   req/s |       `N/A`   req/s |       `N/A`   req/s |
| ![](https://img.shields.io/static/v1?label=mdathx&message=5ed4ec&color=blue&logo=nginx&logoColor=009639)        |   `1874.55`   req/s |   `1610.93`   req/s |   `1486.78`   req/s |
| ![](https://img.shields.io/static/v1?label=nginx&message=v1.18.0&color=blue&logo=nginx&logoColor=009639)        |   `1910.98`   req/s |   `1655.27`   req/s |   `1524.47`   req/s |

## References

### Related Benchmarks

- [Benchmark](https://github.com/mangadex-network/webframework-benchmarks/blob/master/BENCHMARKS.md) of selected Web-Frameworks
- [Benchmark Results](https://_tde9.gitlab.io/mdah-bench/) from other [GitLab Repository](https://gitlab.com/_tde9/mdah-bench)

### Client Implementations

- [mdath](https://gitlab.com/mangadex-pub/mangadex_at_home) (official client, by MangaDex Developer • __carbotaniuman__)
- [kmdah](https://github.com/Tristan971/kmdah) (upstream server, by MangaDex Developer • __Tristan971__)
- [mdath-go](https://github.com/lflare/mdathome-golang) (by MangaDex Developer • __lflare__)
- [scalpel](https://github.com/DevBlocky/scalpel) (by __DevBlocky__)
- [mdath-rs](https://github.com/edward-shen/mangadex-home-rs) (by __edward-shen__)
- [md@cloud](https://www.npmjs.com/package/@mangadex/cloud) (by __ronny1982__)
- [cheetah](https://github.com/mangadex-network/cheetah) (by __ronny1982__)
- [mdathx](https://github.com/mangadex-network/mdathx) (by __ronny1982__)
