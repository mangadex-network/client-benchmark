stages:
  - Benchmark-SSL
  - Benchmark-NoSSL

################
### MD@CLOUD ###
################

MD@CLOUD.SSL:
  when: manual
  stage: Benchmark-SSL
  image: node:latest
  script: |
        apt-get update
        apt-get install -y unzip apache2-utils ca-certificates
        curl -L "https://github.com/mangadex-network/mangadex-at-cloud/archive/refs/heads/node.zip" > mangadex-at-cloud-node.zip
        unzip mangadex-at-cloud-node.zip
        cd mangadex-at-cloud-node
        npm install
        npm run build:debug && node ./test/serve.js &> ../mdatcloud.ssl.log &
        sleep 10
        HOST="Host: localhost.mangadex.network"
        URL="https://127.0.0.1:44300/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 -H "$HOST" "$URL"
        sleep 1
        ab -n 1000 -c 10 -H "$HOST" "$URL" | tee ../results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 -H "$HOST" "$URL" | tee ../results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 -H "$HOST" "$URL" | tee ../results.ssl.200.txt
  artifacts:
    paths:
      - mdatcloud.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

###############
### CHEETAH ###
###############

CHEETAH.SSL:
  when: manual
  stage: Benchmark-SSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        chmod +x cheetah
        ./cheetah --key=$CLIENT_KEY --port=44300 --no-token-check --cache=./cache &> cheetah.ssl.log &
        sleep 10
        URL="https://127.0.0.1:44300/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.ssl.200.txt
  artifacts:
    paths:
      - cheetah.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

CHEETAH.NoSSL:
  when: manual
  stage: Benchmark-NoSSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        chmod +x cheetah
        ./cheetah cache --port=8000 --upstream=https://uploads.mangadex.org --cache=./cache &> cheetah.nossl.log &
        sleep 10
        URL="http://127.0.0.1:8000/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 2500 -c 5 "$URL"
        sleep 1
        ab -n 5000 -c 10 "$URL" | tee results.nossl.10.txt
        sleep 1
        ab -n 5000 -c 100 "$URL" | tee results.nossl.100.txt
        sleep 1
        ab -n 5000 -c 200 "$URL" | tee results.nossl.200.txt
  artifacts:
    paths:
      - cheetah.nossl.log
      - results.nossl.10.txt
      - results.nossl.100.txt
      - results.nossl.200.txt

################
### MDATH-GO ###
################

MDATH-GO.SSL:
  when: manual
  stage: Benchmark-SSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        echo '{' > settings.json
        echo '"cache_directory": "./cache/",' >> settings.json
        echo '"client_port": 44300,' >> settings.json
        echo \"client_secret\": \"$CLIENT_KEY\", >> settings.json
        echo '"max_reported_size_in_mebibytes": 65536,' >> settings.json
        echo '"reject_invalid_tokens": false' >> settings.json
        echo '}' >> settings.json
        chmod +x mdath-go
        ./mdath-go &> mdath-go.ssl.log &
        sleep 10
        URL="https://127.0.0.1:44300/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.ssl.200.txt
  artifacts:
    paths:
      - mdath-go.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

###############
### SCALPEL ###
###############

SCALPEL.SSL:
  when: manual
  stage: Benchmark-SSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        echo "### CLIENT CONFIGURATION ###" > settings.yaml
        echo "skip_tokens: true" >> settings.yaml
        echo "max_grace_period: 0" >> settings.yaml
        echo "cache_size_mebibytes: 65536" >> settings.yaml
        echo "cache_engine: rocksdb" >> settings.yaml
        echo "fs_options:" >> settings.yaml
        echo "    path: ./cache" >> settings.yaml
        echo "rocksdb_options:" >> settings.yaml
        echo "    path: ./cache" >> settings.yaml
        echo "bind_address: 127.0.0.1" >> settings.yaml
        echo "keep_alive: 10" >> settings.yaml
        echo "enforce_secure_tls: false" >> settings.yaml
        echo "disable_ad_headers: false" >> settings.yaml
        echo "reject_invalid_sni: false" >> settings.yaml
        echo "client_secret: $CLIENT_KEY" >> settings.yaml
        echo "port: 44300" >> settings.yaml
        echo "disable_ssl: false" >> settings.yaml
        chmod +x scalpel
        ./scalpel &> scalpel.ssl.log &
        sleep 10
        URL="https://127.0.0.1:44300/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.ssl.200.txt
  artifacts:
    paths:
      - scalpel.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

SCALPEL.NoSSL:
  when: manual
  stage: Benchmark-NoSSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        echo "### CLIENT CONFIGURATION ###" > settings.yaml
        echo "skip_tokens: true" >> settings.yaml
        echo "max_grace_period: 0" >> settings.yaml
        echo "cache_size_mebibytes: 65536" >> settings.yaml
        echo "cache_engine: rocksdb" >> settings.yaml
        echo "fs_options:" >> settings.yaml
        echo "  path: ./cache" >> settings.yaml
        echo "rocksdb_options:" >> settings.yaml
        echo "  path: ./cache" >> settings.yaml
        echo "bind_address: 127.0.0.1" >> settings.yaml
        echo "keep_alive: 10" >> settings.yaml
        echo "enforce_secure_tls: false" >> settings.yaml
        echo "disable_ad_headers: false" >> settings.yaml
        echo "reject_invalid_sni: false" >> settings.yaml
        echo "client_secret: $CLIENT_KEY" >> settings.yaml
        echo "port: 8000" >> settings.yaml
        echo "disable_ssl: true" >> settings.yaml
        chmod +x scalpel
        ./scalpel &> scalpel.nossl.log &
        sleep 10
        URL="http://127.0.0.1:8000/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 2500 -c 5 "$URL"
        sleep 1
        ab -n 5000 -c 10 "$URL" | tee results.nossl.10.txt
        sleep 1
        ab -n 5000 -c 100 "$URL" | tee results.nossl.100.txt
        sleep 1
        ab -n 5000 -c 200 "$URL" | tee results.nossl.200.txt
  artifacts:
    paths:
      - scalpel.nossl.log
      - results.nossl.10.txt
      - results.nossl.100.txt
      - results.nossl.200.txt

################
### MDATH-RS ###
################

MDATH-RS.SSL:
  when: manual
  stage: Benchmark-SSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        echo "### CLIENT CONFIGURATION ###" > settings.yaml
        echo "max_cache_size_in_mebibytes: 65536" >> settings.yaml
        echo "server_settings:" >> settings.yaml
        echo "  external_max_kilobits_per_second: 1" >> settings.yaml
        echo "  secret: $CLIENT_KEY" >> settings.yaml
        echo "  port: 44300" >> settings.yaml
        chmod +x mdath-rs
        ./mdath-rs --unstable-options disable-token-validation &> mdath-rs.ssl.log &
        sleep 10
        URL="https://127.0.0.1:44300/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.ssl.200.txt
  artifacts:
    paths:
      - mdath-rs.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

MDATH-RS.NoSSL:
  when: manual
  stage: Benchmark-NoSSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        echo "### CLIENT CONFIGURATION ###" > settings.yaml
        echo "max_cache_size_in_mebibytes: 65536" >> settings.yaml
        echo "server_settings:" >> settings.yaml
        echo "  external_max_kilobits_per_second: 1" >> settings.yaml
        echo "  secret: $CLIENT_KEY" >> settings.yaml
        echo "  port: 8000" >> settings.yaml
        chmod +x mdath-rs
        # TODO: how did these unstable options work ...
        ./mdath-rs --unstable-options disable-tls disable-token-validation &> mdath-rs.nossl.log &
        sleep 10
        URL="http://127.0.0.1:8000/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 2500 -c 5 "$URL"
        sleep 1
        ab -n 5000 -c 10 "$URL" | tee results.nossl.10.txt
        sleep 1
        ab -n 5000 -c 100 "$URL" | tee results.nossl.100.txt
        sleep 1
        ab -n 5000 -c 200 "$URL" | tee results.nossl.200.txt
  artifacts:
    paths:
      - mdath-rs.nossl.log
      - results.nossl.10.txt
      - results.nossl.100.txt
      - results.nossl.200.txt

#############
### KMDAH ###
#############

KMDAH.NoSSL:
  when: manual
  stage: Benchmark-NoSSL
  image: adoptopenjdk:16
  variables:
    JAVA_TOOL_OPTIONS: "-Xms768M -Xmx768M -Xss512K -XX:+UseShenandoahGC"
    HEALTH_ENDPOINT: "/__mon/health"
  script: |
        apt-get update
        apt-get install -y libsodium23 redis apache2-utils ca-certificates curl
        redis-server &> /dev/null &
        echo "server.jetty.threads:" > config.yml
        echo "  min: 8" >> config.yml
        echo "  max: 8" >> config.yml
        echo "kmdah:" >> config.yml
        echo "  cache:" >> config.yml
        echo "    backend: filesystem" >> config.yml
        echo "    max-size-gb: 64" >> config.yml
        echo "    abort-lookup-threshold-millis: 500" >> config.yml
        echo "    filesystem:" >> config.yml
        echo "      root-dir: /tmp/cache" >> config.yml
        echo "  tls:" >> config.yml
        echo "    backend: file" >> config.yml
        echo "  mangadex:" >> config.yml
        echo "    client-secret: $CLIENT_KEY" >> config.yml
        echo "    load-balancer-ip: 1.1.1.1" >> config.yml
        echo "    enforce-tokens: false" >> config.yml
        echo "    bandwidth-mbps: 10" >> config.yml
        echo "  worker:" >> config.yml
        echo "    port: 8000" >> config.yml
        mkdir -pv /tmp/cache
        chmod +x kmdah.jar
        java -jar kmdah.jar &> kmdah.nossl.log &
        sleep 25
        URL="http://127.0.0.1:8000/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.nossl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.nossl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.nossl.200.txt
  artifacts:
    paths:
      - kmdah.nossl.log
      - results.nossl.10.txt
      - results.nossl.100.txt
      - results.nossl.200.txt

#############
### MDATH ###
#############

MDATH.SSL:
  when: manual
  stage: Benchmark-SSL
  image: adoptopenjdk:16
  variables:
    JAVA_TOOL_OPTIONS: "-Xms768M -Xmx768M -Xss512K -XX:+UseShenandoahGC"
  script: |
        apt-get update
        apt-get install -y apache2-utils ca-certificates
        echo "max_cache_size_in_mebibytes: 65536" > settings.yaml
        echo "server_settings:" >> settings.yaml
        echo "  secret: "$CLIENT_KEY"" >> settings.yaml
        echo "  port: 44300" >> settings.yaml
        echo "  hostname: 0.0.0.0" >> settings.yaml
        chmod +x mdath.jar
        java -jar mdath.jar &> mdath.ssl.log &
        sleep 10
        URL="https://127.0.0.1:44300/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.ssl.200.txt
  artifacts:
    paths:
      - mdath.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

##############
### MDATHX ###
##############

MDATHX.SSL:
  when: manual
  stage: Benchmark-SSL
  image: node:latest
  script: |
        apt-get update
        apt-get install -y nginx apache2-utils ca-certificates
        chmod +x mdathx
        ./mdathx --key=$CLIENT_KEY --port=44300 --cache=./cache --size=64 &> mdathx.ssl.log &
        sleep 10
        URL="https://127.0.0.1:44300/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.ssl.200.txt
  artifacts:
    paths:
      - mdathx.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

MDATHX.NoSSL:
  when: manual
  stage: Benchmark-NoSSL
  image: node:latest
  script: |
        apt-get update
        apt-get install -y nginx apache2-utils ca-certificates
        chmod +x mdathx
        ./mdathx --key=$CLIENT_KEY --port=8000 --cache=./cache --size=64 --no-ssl &> mdathx.nossl.log &
        sleep 10
        URL="http://127.0.0.1:8000/-/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.nossl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.nossl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.nossl.200.txt
  artifacts:
    paths:
      - mdathx.nossl.log
      - results.nossl.10.txt
      - results.nossl.100.txt
      - results.nossl.200.txt

#############
### NGINX ###
#############

NGINX.SSL:
  when: manual
  stage: Benchmark-SSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y nginx apache2-utils ca-certificates
        nginx -p ./nginx -c localhost-ssl.conf
        sleep 10
        URL="https://127.0.0.1:44300/sample.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.ssl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.ssl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.ssl.200.txt
  artifacts:
    paths:
#      - nginx.ssl.log
      - results.ssl.10.txt
      - results.ssl.100.txt
      - results.ssl.200.txt

NGINX.NoSSL:
  when: manual
  stage: Benchmark-NoSSL
  image: ubuntu:latest
  script: |
        apt-get update
        apt-get install -y nginx apache2-utils ca-certificates
        nginx -p ./nginx -c localhost-nossl.conf
        sleep 10
        URL="http://127.0.0.1:8000/sample.png"
        # warmup
        ab -n 500 -c 5 "$URL"
        sleep 1
        ab -n 1000 -c 10 "$URL" | tee results.nossl.10.txt
        sleep 1
        ab -n 1000 -c 100 "$URL" | tee results.nossl.100.txt
        sleep 1
        ab -n 1000 -c 200 "$URL" | tee results.nossl.200.txt
  artifacts:
    paths:
#      - nginx.nossl.log
      - results.nossl.10.txt
      - results.nossl.100.txt
      - results.nossl.200.txt
